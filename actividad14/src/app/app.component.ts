import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PhotosService } from './services/photos/photos.service';
import { PostsService } from './services/posts/posts.service';
import { UsersService } from './services/users/users.service';
import { GetService } from './services/get/get.service';
import { PostService } from './services/post/post.service';
import { PutService } from './services/put/put.service';
import { PatchService } from './services/patch/patch.service';
import { DeleteService } from './services/delete/delete.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'actividad14';

  posts: any;
  photos: any;
  users: any;
  public pagePosts!: number;
  public pagePhotos!: number;
  public pageUsers!: number;

  getAnswer: any;
  postAnswer: any;
  putAnswer: any;
  patchAnswer: any;
  deleteAnswer: boolean = false;
  getForm!: FormGroup;
  postForm!: FormGroup;
  putForm!: FormGroup;
  patchForm!: FormGroup;
  deleteForm!: FormGroup;

  constructor(
    public fb: FormBuilder,
    public photosService: PhotosService,
    public postsService: PostsService,
    public usersService: UsersService,
    public getService: GetService,
    public postService: PostService,
    public putService: PutService,
    public patchService: PatchService,
    public deleteService: DeleteService
  ) {}

  ngOnInit(): void {

    this.postsService.getAllPosts().subscribe(
      response => { this.posts = response; },
      error => { console.error(error); }
    );

    this.photosService.getAllPhotos().subscribe(
      response => { this.photos = response; },
      error => { console.error(error); }
    );

    this.usersService.getAllUsers().subscribe(
      response => { this.users = response; },
      error => { console.error(error); }
    );

    this.getForm = this.fb.group ({
      id: ['', Validators.required]
    });

    this.postForm = this.fb.group ({
      userId: ['', Validators.required],
      title: ['', Validators.required],
      body: ['', Validators.required]
    });
    
    this.putForm = this.fb.group ({
      userId: ['', Validators.required],
      title: ['', Validators.required],
      body: ['', Validators.required]
    });

    this.patchForm = this.fb.group ({
      id: ['', Validators.required],
      userId: [''],
      title: [''],
      body: ['']
    });

    this.deleteForm = this.fb.group ({
      id: ['', Validators.required]
    });

  }

  get(): void {
    this.getService.get(this.getForm.value.id).subscribe(
      response => { this.getAnswer = response; },
      error => { console.error(error); }
    );
  }

  postFunction(): void {
    this.postService.post(this.postForm.value).subscribe(
      response => { this.postAnswer = response; },
      error => { console.error(error); }
    );
  }

  put(): void {
    this.putService.put(this.putForm.value.userId,  this.putForm.value).subscribe(
      response => { this.putAnswer = response; },
      error => { console.error(error); }
    );
  }

  patch(): void {
    this.patchService.patch(this.patchForm.value.id,  this.patchForm.value).subscribe(
      response => { this.patchAnswer = response; },
      error => { console.error(error); }
    );
  }

  delete(): void {
    this.deleteAnswer = false;
    this.deleteService.delete(this.deleteForm.value.id).subscribe(
      response => { this.deleteAnswer = true; },
      error => { console.error(error); }
    );
  }

}
