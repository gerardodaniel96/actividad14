import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  private API_SERVER = "https://jsonplaceholder.typicode.com/posts/";

  constructor(private httpClient: HttpClient) { }

  public post(body: any) : Observable<any> {
    return this.httpClient.post(this.API_SERVER, body);
  }

}
