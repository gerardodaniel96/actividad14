import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PatchService {

  private API_SERVER = "https://jsonplaceholder.typicode.com/posts/";

  constructor(private httpClient: HttpClient) { }

  public patch(id: number, body: any) : Observable<any> {
    return this.httpClient.patch(this.API_SERVER + id, body);
  }
  
}
