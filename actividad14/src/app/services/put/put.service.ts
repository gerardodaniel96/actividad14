import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PutService {

  private API_SERVER = "https://jsonplaceholder.typicode.com/posts/";

  constructor(private httpClient: HttpClient) { }

  public put(id: number, body: any) : Observable<any> {
    return this.httpClient.put(this.API_SERVER + id, body);
  }

}
