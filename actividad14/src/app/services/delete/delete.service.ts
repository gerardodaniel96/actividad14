import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DeleteService {

  private API_SERVER = "https://jsonplaceholder.typicode.com/posts/";

  constructor(private httpClient: HttpClient) { }

  public delete(id: number) : Observable<any> {
    return this.httpClient.delete(this.API_SERVER + id);
  }

}
