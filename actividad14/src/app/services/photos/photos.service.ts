import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PhotosService {

  private API_SERVER = "https://jsonplaceholder.typicode.com/photos";

  constructor(private httpClient: HttpClient) { }

  public getAllPhotos() : Observable<any> {
    return this.httpClient.get(this.API_SERVER);
  }

}
