import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GetService {

  private API_SERVER = "https://jsonplaceholder.typicode.com/posts/";

  constructor(private httpClient: HttpClient) { }

  public get(id: number) : Observable<any> {
    return this.httpClient.get(this.API_SERVER + id);
  }

}
