import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private API_SERVER = "https://jsonplaceholder.typicode.com/users";

  constructor(private httpClient: HttpClient) { }

  public getAllUsers() : Observable<any> {
    return this.httpClient.get(this.API_SERVER);
  }

}
